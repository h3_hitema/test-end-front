// src/App.js
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import './App.css'; // Ajoutez ce fichier CSS

function App() {
  const [users, setUsers] = useState([]);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await axios.get('http://localhost:3001/users');
        setUsers(response.data);
      } catch (error) {
        console.error('Error fetching users:', error);
      }
    };

    fetchUsers();
  }, []);

  return (
      <div className="App">
        <h1>Trombinoscope End</h1>
        <div className="user-table" style={{background: 'red'}}>
          {users.map((user, index) => (
              <div key={user.id} className="user-row">
                <img
                    src={user.image}
                    alt={`${user.lastname} ${user.firstname}`}
                    style={{ width: '100px', height: '100px', borderRadius: '50%' }}
                />
                <div className="user-info">
                  <p>{user.lastname}</p>
                  <p>{user.firstname}</p>
                </div>
                {(index + 1) % 5 === 0 && <div className="clear-row" />}
              </div>
          ))}
        </div>
      </div>
  );
}

export default App;
