# Utilisez une image Node.js comme base
FROM node:14

# Définissez le répertoire de travail dans le conteneur
WORKDIR /usr/src/app

# Copiez le fichier package.json et package-lock.json
COPY package*.json ./

# Installez les dépendances
RUN npm install

# Copiez le reste des fichiers du projet
COPY . .

# Construisez l'application React pour la production
RUN npm run build

# Port sur lequel l'application va écouter
EXPOSE 3000

# Commande pour exécuter l'application
CMD ["npm", "start"]
